import java.util.Scanner;  // Import the Scanner class

public class Palindrome {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter string to verify if palindrome:");
        String original = myObj.nextLine();  // Reads user input
        while (original.isEmpty() || original==null){
            System.out.println("Please enter a string to verify if paindrome. String cannot be empty");
            original = myObj.nextLine();
        }
        original = original.replaceAll("[^a-zA-Z0-9]+", "").toLowerCase();
        int length = original.length();
        String reverse ="";
        for (int x =length-1; x>=0; x-- ){
            reverse =reverse + original.charAt(x);
        }
        if (original.equals(reverse.toLowerCase())){
            System.out.println("This string is palindrome");
        }
        else {
            System.out.println("This string is not palindrome");
        }

    }
}