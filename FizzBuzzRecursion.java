public class FizzBuzzRecursion { // 07/24/19 bkang
    static int x = 0;

    public static void fizzBuzz() {
        x++;
        if (x <= 100) {
            if (x % 15 == 0) {
                System.out.println("FizzBuzz");
            } else if (x % 5 == 0) {
                System.out.println("Buzz");
            } else if (x % 3 == 0) {
                System.out.println("Fizz");
            } else {
                System.out.println(x);
            }
            fizzBuzz();
        }
    }
    public static void main(String[] args) {
        fizzBuzz();
    }
}