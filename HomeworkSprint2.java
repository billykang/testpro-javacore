import java.util.Scanner;  // Import the Scanner class

public class HomeworkSprint2 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter string");
        String string = myObj.nextLine();  // Reads user input

        if ((string.length() % 2 == 0)){
            System.out.println("String is even");
        } else {
            System.out.println("String is not even");
        }
    }
}
